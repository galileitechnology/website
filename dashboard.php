<?php
session_start();
if(!isset($_SESSION['iduser']))
{
    header("location: galileilogin.php");
    exit;
}
?>
<html lang="en">
<meta charset="utf-8">
<link rel="icon" href="./galilei-site-logo.svg">
<script src="./dashboard.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="./dashboard.css">
<link href="https://fonts.googleapis.com/css2?family=Jura&family=Roboto:wght@300&display=swap" rel="stylesheet">
    <head>
        <title>Dashboard | Galilei</title>
    </head>
    <body>
        <a href="./galilei.php"><img class="home" src="./galilei-site-logo.svg"></a>
        <div class="sidetabdiv">
            <p class="sidetab">

            </p>
        </div>
            <div class="workspace">
                <a href="./workspace/workspace.html">
                    <p class="workspacetabtext">
                        Galilei Workspace
                    </p>
                </a>
            </div>
            </a>
            <div class="safe">
                <a href="./safe/safe.html">
                    <p class="safetabtext">
                        Galilei Safe
                    </p>
                </a>
            </div>
            </a>
            <div class="stryder">
                <a href="./stryderx/stryder.html">
                    <p class="strydertabtext">
                        Galilei Stryder
                    </p>
                </a>
            </div>
            <div class="razor">
                <a href="./razor/razor.html">
                    <p class="razortabtext">
                        Galilei Razor
                    </p>
                </a>
            </div>
    </body>
</html>